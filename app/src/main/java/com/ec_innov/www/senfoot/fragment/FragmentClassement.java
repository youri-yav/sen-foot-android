package com.ec_innov.www.senfoot.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.ec_innov.www.senfoot.Adapter.GridViewAdapterClassement;
import com.ec_innov.www.senfoot.Adapter.GridViewAdapterEquipe;
import com.ec_innov.www.senfoot.Classes.Classement;
import com.ec_innov.www.senfoot.Classes.Config;
import com.ec_innov.www.senfoot.Classes.Equipe;
import com.ec_innov.www.senfoot.Classes.Function;
import com.ec_innov.www.senfoot.Interfaces.FriendlyService;
import com.ec_innov.www.senfoot.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class FragmentClassement extends Fragment {
    private GridView gridView;
    SwipeRefreshLayout mSwipeRefreshLayout;
    //Tag values to read from json
    GridViewAdapterClassement gridViewAdapter;
    ArrayList<Classement> listeClassement;
    Classement classement;
    TextView txtErrox;
    public FragmentClassement() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_classement, container, false);
        gridView = (GridView) view.findViewById(R.id.gridView);
        txtErrox=(TextView) view.findViewById(R.id.noNetWorking);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.activity_main_swipe_refresh_layout);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {

                if(Function.isNetworkRunnig(getContext()))
                {
                    txtErrox.setVisibility(View.INVISIBLE);
                    txtErrox.setText("connexion impossible");
                    getData();

                    //server.execute();
                }
                else
                {
                    txtErrox.setText("");
                    txtErrox.setVisibility(View.VISIBLE);
                }

            }
        });
        listeClassement = new ArrayList<Classement>();
        if(Function.isNetworkRunnig(getContext()))
        {
            txtErrox.setVisibility(View.INVISIBLE);
            mSwipeRefreshLayout.setRefreshing(true);
            getData();

            //server.execute();
        }
        else
        {
            txtErrox.setVisibility(View.VISIBLE);
        }
        gridViewAdapter = new GridViewAdapterClassement(getActivity().getApplicationContext(),listeClassement);
        gridView.setAdapter(gridViewAdapter);
        return view;
    }

    private void getData(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        FriendlyService  service = retrofit.create(FriendlyService.class);
        final Call<List<Classement>> tmp = service.getClassement();
        tmp.enqueue(new Callback<List<Classement>>() {
            @Override
            public void onResponse(Call<List<Classement>> call, Response<List<Classement>> response) {
                Log.d("mytestRes","good!! "+response.code()+toString());
                if(response.code()==200)
                {
                    listeClassement.clear();
                    for(Classement classement:response.body())
                    {
                        listeClassement.add(classement);
                        Log.d("resultat : ",classement.getEquipe());
                    }
                    gridViewAdapter.notifyDataSetChanged();
                    if(listeClassement.size()==0)
                    {
                        //bloc_no_event.setVisibility(View.VISIBLE);
                    }
                    else {
                        //bloc_no_event.setVisibility(View.INVISIBLE);
                    }
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }
            @Override
            public void onFailure(Call<List<Classement>> call, Throwable t) {
                Log.d("mytestRes",""+t.getMessage());
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

}
