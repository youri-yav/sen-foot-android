package com.ec_innov.www.senfoot.Interfaces;



import com.ec_innov.www.senfoot.Classes.Classement;
import com.ec_innov.www.senfoot.Classes.Config;
import com.ec_innov.www.senfoot.Classes.Equipe;
import com.ec_innov.www.senfoot.Classes.Match;

import org.json.JSONArray;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by mdpcompany on 01/03/2018.
 */

public interface FriendlyService {
    @GET("admin/mobil/get-resultat/")
    Call<List<Match>> getResult();

    @GET("admin/mobil/get-calendar/")
    Call<List<Match>> getCalendar();
    @GET("admin/mobil/get-rang/")
    Call<List<Classement>> getClassement();
    @GET("admin/mobil/get-equipes/")
    Call<List<Equipe>> getListeEquipe();

    /*@FormUrlEncoded
    @POST("mobil/register")
    Call<Login> register(@Field("nom") String nom, @Field("prenom") String prenom, @Field("email") String email, @Field("phone") String phone, @Field("login") String login, @Field("password") String password);

    @FormUrlEncoded
    @POST("mobil/create-profil")
    Call<String> createProfil(@Field("profil") String profil, @Field("idMembre") String idMembre, @Field("sexe") String sexe, @Field("pays") String pays, @Field("religion") String religion, @Field("profession") String profession, @Field("dateNaiss") String dateNaiss, @Field("centre") String centre);

    @GET("mobil/get-data-for-register")
    Call<ResponseBody> getDataForProfil();

    @GET("mobil/get-list-of-type-abonement")
    Call<Abonement> getListOfAbonement();

    @FormUrlEncoded
    @POST("mobil/payement")
    Call<InfoUser> payement(@Field("idTypeAbonnement") String idTypeAbonnement, @Field("idMembre") String idMembre, @Field("idTxn") String idTxn);

    @GET("mobil/get-membre/{id}")
    Call<List<Membre>> getListOfUsers(@Path("id") String id);
    @GET("mobil/get-event/{id}")
    Call<List<Evenement>> getEvent(@Path("id") String id);

    @GET("mobil/get-my-event/{id}")
    Call<List<Evenement>> getMyEvents(@Path("id") String id);

    @FormUrlEncoded
    @POST("mobil/new-evenement")
    Call<String> newEvent(@Field("libelle") String libelle, @Field("categorie") String categorie, @Field("description") String description, @Field("adresse") String adresse,
                          @Field("nbrInvite") String nbrInvite, @Field("date") String date, @Field("dateRes") String dateRes, @Field("heure") String heure, @Field("listeMembre")
                                  String listeMembre, @Field("longitude") String longitude, @Field("latitude") String latitude, @Field("membre") String membre,
                          @Field("image") String image1, @Field("image2") String image2, @Field("image3") String image3, @Field("image4") String image4);

    @GET("mobil/reinit-password/{login}")
    Call<String> reinitPassword(@Path("login") String login);

    @GET("mobil/request-invitation/{idEvent}/{membre}/{type}")
    Call<String> requeInvitation(@Path("idEvent") String idEvent, @Path("membre") String membre, @Path("type") String type);

    @GET("mobil/get-invite/{id}")
    Call<List<Membre>> getInvites(@Path("id") String idEvent);

    @GET("mobil/annuler-evenement/{id}/{idMembre}")
    Call<String> annulerEvent(@Path("id") String idEvent, @Path("idMembre") String idMembre);

    @GET("mobil/get-message/{idEvent}/{membre}/{invite}")
    Call<List<Message>> getMessage(@Path("idEvent") String idEvent, @Path("membre") String membre, @Path("invite") String invite);

    @GET("mobil/update-message/{idEvent}/{membre}/{invite}/{lasteMessageId}")
    Call<List<Message>> updateMessage(@Path("idEvent") String idEvent, @Path("membre") String membre, @Path("invite") String invite, @Path("lasteMessageId") String lasteMessageId);



    @GET("mobil/get-my-event/{code}/{eventId}")
    Call<String> checkInvite(@Path("code") String code, @Path("eventId") String eventId);
*/

}
