package com.ec_innov.www.senfoot.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.ec_innov.www.senfoot.Classes.Match;
import com.ec_innov.www.senfoot.Classes.Produit;
import com.ec_innov.www.senfoot.R;

import java.util.ArrayList;

/**
 * Created by cartalinkPc on 31/01/2017.
 */

public class GridViewAdapter extends BaseAdapter {
    //Imageloader to load images

    ImageView logoVisit;
    ImageView logoHome;
    TextView txtLibelleHome;
    TextView txtScoreHome;
    TextView txtLibelleVisit;
    TextView txtScoreVisit;
    TextView txtDateMatch;
    byte[] decodedString;
    Bitmap decodedByte;
    Match resultat;

    //Context
    private Context context;

    //Array List that would contain the urls and the titles for the images
    private ArrayList<Match> resultats;
    private LayoutInflater inflater;
    View vue;
    public GridViewAdapter(Context context, ArrayList<Match> resultats){
        //Getting all the values
        this.context = context;
        this.resultats = resultats;
        inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return resultats.size();
    }

    @Override
    public Object getItem(int position) {
        return resultats.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Creating a linear layout
        vue = this.inflater.inflate(R.layout.custum_grid, null);
        resultat=this.resultats.get(position);
        logoVisit=(ImageView)vue.findViewById(R.id.logoVisit);
        logoHome=(ImageView)vue.findViewById(R.id.logoHome);
        txtLibelleHome=(TextView) vue.findViewById(R.id.txtLibelleHome);
        txtScoreHome=(TextView) vue.findViewById(R.id.txtScoreHome);
        txtScoreVisit=(TextView) vue.findViewById(R.id.txtScoreVisit);
        txtLibelleVisit=(TextView) vue.findViewById(R.id.txtLibelleVisit);

        txtDateMatch=(TextView) vue.findViewById(R.id.txtDateMatch);
        txtLibelleHome.setText(resultat.getHomeTeam());
        txtLibelleVisit.setText(resultat.getVisiTeam());
        txtScoreVisit.setText(String.valueOf(resultat.getButVisiTeam()));
        txtScoreHome.setText(String.valueOf(resultat.getButHomeTeam()));
        txtDateMatch.setText(resultat.getDateMatch());

        decodedString = Base64.decode(resultat.getLogoHomeTeam(), Base64.DEFAULT);
        decodedByte= BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        logoHome.setImageBitmap(decodedByte);
        decodedString=null;
        decodedByte=null;
        decodedString = Base64.decode(resultat.getLogoVisiTeam(), Base64.DEFAULT);
        decodedByte= BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        logoVisit.setImageBitmap(decodedByte);
        decodedString=null;
        decodedByte=null;
        return vue;
    }
}
