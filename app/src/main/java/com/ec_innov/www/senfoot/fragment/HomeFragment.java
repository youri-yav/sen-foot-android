package com.ec_innov.www.senfoot.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.ec_innov.www.senfoot.Adapter.GridViewAdapter;
import com.ec_innov.www.senfoot.Classes.Config;
import com.ec_innov.www.senfoot.Classes.Function;
import com.ec_innov.www.senfoot.Classes.Match;
import com.ec_innov.www.senfoot.Classes.Produit;
import com.ec_innov.www.senfoot.Interfaces.FriendlyService;
import com.ec_innov.www.senfoot.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class HomeFragment extends Fragment {
    private GridView gridView;
    SwipeRefreshLayout mSwipeRefreshLayout;
    public static final String DATA_URL = "http://www.simplifiedcodingreaders.16mb.com/superheroes.php";
    //Tag values to read from json
    public static final String TAG_IMAGE_URL = "image";
    public static final String TAG_NAME = "name";
    private ArrayList<String> images;
    private ArrayList<String> names;
    GridViewAdapter gridViewAdapter;
    ArrayList<Match> listeResultats;
    Produit produit;
    TextView txtErrox;
    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_home_layout, container, false);
        gridView = (GridView) view.findViewById(R.id.gridView);
        txtErrox=(TextView) view.findViewById(R.id.noNetWorking);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.activity_main_swipe_refresh_layout);

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimary, R.color.colorPrimary);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {

                if(Function.isNetworkRunnig(getContext()))
                {
                    txtErrox.setVisibility(View.INVISIBLE);
                    getData();

                    //server.execute();
                }
                else
                {
                    txtErrox.setVisibility(View.VISIBLE);
                }

            }
        });
        listeResultats = new ArrayList<Match>();
        if(Function.isNetworkRunnig(getContext()))
        {
            txtErrox.setVisibility(View.INVISIBLE);
            mSwipeRefreshLayout.setRefreshing(true);
            getData();

            //server.execute();
        }
        else
        {
            txtErrox.setVisibility(View.VISIBLE);
        }
        gridViewAdapter = new GridViewAdapter(getActivity().getApplicationContext(),listeResultats);
        gridView.setAdapter(gridViewAdapter);
        return view;
    }

    private void getData(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.HOST)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        FriendlyService  service = retrofit.create(FriendlyService.class);
        final Call<List<Match>> tmp = service.getResult();
        tmp.enqueue(new Callback<List<Match>>() {
            @Override
            public void onResponse(Call<List<Match>> call, Response<List<Match>> response) {
                Log.d("mytestRes","good!! "+response.code()+toString());
                if(response.code()==200)
                {
                    listeResultats.clear();
                    for(Match resultat:response.body())
                    {
                        listeResultats.add(resultat);
                        Log.d("resultat : ",resultat.getHomeTeam());
                    }
                    gridViewAdapter.notifyDataSetChanged();
                    if(listeResultats.size()==0)
                    {
                        //bloc_no_event.setVisibility(View.VISIBLE);
                    }
                    else {
                        //bloc_no_event.setVisibility(View.INVISIBLE);
                    }
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<List<Match>> call, Throwable t) {
                Log.d("mytestRes",""+t.getMessage());
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }


    private void showGrid(JSONArray jsonArray){
        //Looping through all the elements of json array
        for(int i = 0; i<jsonArray.length(); i++){
            //Creating a json object of the current index
            JSONObject obj = null;
            try {
                //getting json object from current index
                obj = jsonArray.getJSONObject(i);

                //getting image url and title from json object
                images.add(obj.getString(TAG_IMAGE_URL));
                names.add(obj.getString(TAG_NAME));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        //Creating GridViewAdapter Object


        //Adding adapter to gridview
       // gridView.setAdapter(gridViewAdapter);
    }

//
}
