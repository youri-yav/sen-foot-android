package com.ec_innov.www.senfoot.Classes;

/**
 * Created by cartalinkPc on 01/02/2017.
 */

public class Produit {
    private String nom;
    private String Descrition;
    private int prix;
    private int image;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescrition() {
        return Descrition;
    }

    public void setDescrition(String descrition) {
        Descrition = descrition;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
