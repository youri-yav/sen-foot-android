package com.ec_innov.www.senfoot.Classes;

public class Match {
    private int idMatch;
    private String HomeTeam;
    private String VisiTeam;
    private String logoVisiTeam;
    private String logoHomeTeam;
    private int butHomeTeam;
    private int butVisiTeam;
    private String dateMatch;

    public int getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(int idMatch) {
        this.idMatch = idMatch;
    }

    public String getHomeTeam() {
        return HomeTeam;
    }

    public void setHomeTeam(String homeTeam) {
        HomeTeam = homeTeam;
    }

    public String getVisiTeam() {
        return VisiTeam;
    }

    public void setVisiTeam(String visiTeam) {
        VisiTeam = visiTeam;
    }

    public String getLogoVisiTeam() {
        return logoVisiTeam;
    }

    public void setLogoVisiTeam(String logoVisiTeam) {
        this.logoVisiTeam = logoVisiTeam;
    }

    public String getLogoHomeTeam() {
        return logoHomeTeam;
    }

    public void setLogoHomeTeam(String logoHomeTeam) {
        this.logoHomeTeam = logoHomeTeam;
    }

    public int getButHomeTeam() {
        return butHomeTeam;
    }

    public void setButHomeTeam(int butHomeTeam) {
        this.butHomeTeam = butHomeTeam;
    }

    public int getButVisiTeam() {
        return butVisiTeam;
    }

    public void setButVisiTeam(int butVisiTeam) {
        this.butVisiTeam = butVisiTeam;
    }

    public String getDateMatch() {
        return dateMatch;
    }

    public void setDateMatch(String dateMatch) {
        this.dateMatch = dateMatch;
    }
}
