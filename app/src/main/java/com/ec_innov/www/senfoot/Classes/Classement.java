package com.ec_innov.www.senfoot.Classes;

public class Classement {
    private String equipe;
    private String logoEquipe;
    private int point;
    private int nbrButMarque;
    private int nbrButEncais;

    public String getEquipe() {
        return equipe;
    }

    public void setEquipe(String equipe) {
        this.equipe = equipe;
    }

    public String getLogoEquipe() {
        return logoEquipe;
    }

    public void setLogoEquipe(String logoEquipe) {
        this.logoEquipe = logoEquipe;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getNbrButMarque() {
        return nbrButMarque;
    }

    public void setNbrButMarque(int nbrButMarque) {
        this.nbrButMarque = nbrButMarque;
    }

    public int getNbrButEncais() {
        return nbrButEncais;
    }

    public void setNbrButEncais(int nbrButEncais) {
        this.nbrButEncais = nbrButEncais;
    }
}
