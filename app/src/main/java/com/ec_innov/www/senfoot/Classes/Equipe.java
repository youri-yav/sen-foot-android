package com.ec_innov.www.senfoot.Classes;

public class Equipe {
    private  int id;
    private  String libelle;
    private  String slogan;
    private  String logoEquipe;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getLogoEquipe() {
        return logoEquipe;
    }

    public void setLogoEquipe(String logoEquipe) {
        this.logoEquipe = logoEquipe;
    }
}
