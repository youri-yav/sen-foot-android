package com.ec_innov.www.senfoot.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ec_innov.www.senfoot.Classes.Equipe;
import com.ec_innov.www.senfoot.Classes.Match;
import com.ec_innov.www.senfoot.R;

import java.util.ArrayList;

/**
 * Created by cartalinkPc on 31/01/2017.
 */

public class GridViewAdapterEquipe extends BaseAdapter {
    //Imageloader to load images
    ImageView logoEquipe;
    TextView txtLibelle;
    byte[] decodedString;
    Bitmap decodedByte;
    Equipe equipe;

    //Context
    private Context context;

    //Array List that would contain the urls and the titles for the images
    private ArrayList<Equipe> equipes;
    private LayoutInflater inflater;
    View vue;
    public GridViewAdapterEquipe(Context context, ArrayList<Equipe> equipes){
        //Getting all the values
        this.context = context;
        this.equipes = equipes;
        inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return equipes.size();
    }

    @Override
    public Object getItem(int position) {
        return equipes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        vue = this.inflater.inflate(R.layout.custum_grid_equipe, null);
        equipe=this.equipes.get(position);
        logoEquipe=(ImageView)vue.findViewById(R.id.logoEquipe);
        txtLibelle=(TextView) vue.findViewById(R.id.txtLibelle);
        txtLibelle.setText(equipe.getLibelle());
        decodedString = Base64.decode(equipe.getLogoEquipe(), Base64.DEFAULT);
        decodedByte= BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        logoEquipe.setImageBitmap(decodedByte);
        decodedString=null;
        decodedByte=null;
        return vue;
    }
}
