package com.ec_innov.www.senfoot.Classes;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ec_innov.www.senfoot.fragment.FragmentCalendier;
import com.ec_innov.www.senfoot.fragment.FragmentClassement;
import com.ec_innov.www.senfoot.fragment.FragmentEquipe;
import com.ec_innov.www.senfoot.fragment.HomeFragment;

/**
 * Created by cartalinkPc on 31/01/2017.
 */

public class Pager extends FragmentStatePagerAdapter {
    private int tabCount;
    public Pager(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount= tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new HomeFragment();
            case 1:
                return new FragmentCalendier();
            case 2:
                return new FragmentClassement();
            case 3:
                return new FragmentEquipe();
            default:
                return null;
        }
        //return null;
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
