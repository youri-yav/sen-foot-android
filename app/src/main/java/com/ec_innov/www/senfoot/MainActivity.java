package com.ec_innov.www.senfoot;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;

import com.ec_innov.www.senfoot.Classes.Pager;

//Implementing the interface OnTabSelectedListener to our MainActivity
//This interface would help in swiping views
public class MainActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener{

    //This is our tablayout
    private TabLayout tabLayout;
    ActionBar actionBar;
    //This is our viewPager
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Sen-Foot");
        //actionBar.setDisplayHomeAsUpEnabled(true);
        //actionBar.setIcon();
        ///actionBar.setDisplayShowHomeEnabled(true);
        //actionBar.hide();
        //actionBar.setIcon(R.drawable.logo_yum_yum);
        Intent intent=getIntent();
        String back=intent.getStringExtra("back");
        //Initializing the tablayout
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        //Adding the tabs using addTab() method
        tabLayout.addTab(tabLayout.newTab().setText("Resultats"));
        tabLayout.addTab(tabLayout.newTab().setText("Calendrier"));
        tabLayout.addTab(tabLayout.newTab().setText("Classement"));
        tabLayout.addTab(tabLayout.newTab().setText("Equipes"));
        //tabLayout.addTab(tabLayout.newTab().setText("Dejener/Diner"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        //Initializing viewPager
        viewPager = (ViewPager) findViewById(R.id.pager);

        //Creating our pager adapter
        Pager adapter = new Pager(getSupportFragmentManager(), tabLayout.getTabCount());

        //Adding adapter to pager
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                //viewPager.setCurrentItem(position);
                Log.d("onPageScrolled",""+position);
            }
            @Override
            public void onPageSelected(int position) {
                Log.d("myposition",""+position);
                tabLayout.getTabAt(position).select();
                loadData(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        //Adding onTabSelectedListener to swipe views
        tabLayout.setOnTabSelectedListener(this);
        if(back!=null)
        {
            tabLayout.getTabAt(2).select();
        }
    }

    private void loadData(int position) {

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent1 = new Intent(MainActivity.this, HomeActivity.class);
                //intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent1);
                overridePendingTransition(R.anim.activity_back_in,R.anim.activity_back_out);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
